public class example {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.print("You didn't enter a name, please try again");
        } else {
            String name = "";
            int index = 0;
            for (String elem : args){
                name += elem + " ";
                index++;
            }
            name = name.substring(0, name.length()-1);
            System.out.print("Hello "
                            + name 
                            + ", your name is " 
                            + (name.length()-(index-1))
                            + " characters long and starts with a "
                            + name.charAt(0)
                            + ".");
        }
    }

}